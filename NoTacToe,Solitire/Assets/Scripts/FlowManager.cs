﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FlowManager : MonoBehaviour {

public Animator MainPanelAnim;
public Button NextBtton;
string State ="";
public static string BoardLevel = "";
public static string GameName = "";

public GameObject SubjectsTodisable;

	#region Sceneflow

 public void GetBoardDetails (string bName)
 {
	 BoardLevel = bName;
	 Initiate.Fade("GamePlay",Color.black,2.0f);
 }
	#endregion

	#region Dataflow
	public GameObject gameSelectionFeild,Menufeild;

	public void OnPlaySelect()
	{
		Menufeild.SetActive(false);
		gameSelectionFeild.SetActive(true);
	}
	public void OnBackSelect()
	{
		gameSelectionFeild.SetActive(false);
		Menufeild.SetActive(true);
	}

	public void OnStudySelect()
	{
		MainPanelAnim.SetBool("DoStudy",true);
	}

	public void OnStudyClose()
	{
		MainPanelAnim.SetBool("DoStudy",false);
	}

	public void ContributionSelect()
	{
		MainPanelAnim.SetBool("Contribution",true);
	}
	public void ContributiondeSelect()
	{
		MainPanelAnim.SetBool("Contribution",false);
	}
	public void WhereToBack()
	{
		Debug.Log("Now the state is "+State);
		if(State.Equals("NoTak"))
		{
			//SubjectsTodisable.SetActive(true);
			
				MainPanelAnim.SetBool("Solitare",false);
				//return;
			
			MainPanelAnim.SetBool("NoTak",false);
			NextBtton.enabled = false;
			
		}
		else if(State.Equals("Solitare"))
		{
			MainPanelAnim.SetBool("Solitare",false);
		}
		else
		{
			OnBackSelect();
		}

		State = "";

	}
	public void NotaktoeSelected()
	{
		//SubjectsTodisable.SetActive(false);
		MainPanelAnim.SetBool("NoTak",true);
		State = "NoTak";
		NextBtton.enabled = true;
	}
	public void SolitareSelected()
	{
		if(State.Equals("NoTak"))
		{
			//MainPanelAnim.speed = 2.0f;
			MainPanelAnim.SetBool("NoTak",false);
		}
		MainPanelAnim.SetBool("Solitare",true);
		State = "Solitare";
		GameName = "Solitare";
	}

public void GameDecided()
{
	GameName = "NoTak";
	if(State.Equals("NoTak"))
		{
			MainPanelAnim.speed = 2.0f;
			MainPanelAnim.SetBool("NoTak",false);
		}
		MainPanelAnim.SetBool("Solitare",true);

}


	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			WhereToBack();
		}
	}

	#endregion
}
