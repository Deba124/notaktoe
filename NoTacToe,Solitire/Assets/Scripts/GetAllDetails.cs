﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetAllDetails : MonoBehaviour {

public Text[] Alldata;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	Alldata[0].text = "Processor: "+SystemInfo.processorType;
	Alldata[1].text = "Processor frequency: "+SystemInfo.processorFrequency +" MHZ";
	Alldata[2].text = "Processor Cores: "+SystemInfo.processorCount +" cores";
	Alldata[3].text = "Total RAM :"+SystemInfo.systemMemorySize +" mb";
	Alldata[4].text = "Total graphic :"+SystemInfo.graphicsMemorySize +" mb";
	Alldata[5].text = "Graphics card :"+SystemInfo.graphicsDeviceName;
	Alldata[6].text = "Graphics API :"+SystemInfo.graphicsDeviceType;
	Alldata[7].text = "Display resolution :"+Screen.currentResolution;
	Alldata[8].text = SystemInfo.deviceModel;
	Alldata[9].text = "Shader model: "+SystemInfo.graphicsShaderLevel.ToString();
	}
}
