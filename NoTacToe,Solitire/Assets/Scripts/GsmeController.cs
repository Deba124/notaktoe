﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GsmeController : MonoBehaviour {
public GameObject GamePanel;
public GameObject Header,Numbered;
public Animator Rules,GameOVR,Shop;
public GameObject ScriptHandler;
public GameObject[] RulesText;
public Text GameText;
public static GameObject GridSelected;
bool allowed = true;
public static bool SinglePlayer;
public static string FristPlayerName;

public Image PlaywithUser,PlaywithComp,TurnUser,TurnOther;

public Sprite normal,selected;

public GameObject TurnStartWith;
void OnEnable()
{
       
	if(SceneManager.GetActiveScene().name.Equals("StartScene"))
	{
		allowed = false;
		Debug.Log("Yess it is");
		return;
	}
	
	if(FlowManager.BoardLevel.Equals("3/3"))
	{
		GamePanel.GetComponent<RectTransform>().GetChild(0).gameObject.SetActive(true);
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(0).gameObject;
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 3 X 3 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 3; SolitaireLogic.col = 3;

		}
			
		else
		{
				GameText.text = "NOTAKTO [ 3 X 3 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
				Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 3;NoTakToLogic.col = 3;

		}
		
	}
	else if(FlowManager.BoardLevel.Equals("4/4"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(1).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(1).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 4 X 4 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 4; SolitaireLogic.col = 4;
		}
			
		else
		{
			GameText.text = "NOTAKTO [ 4 X 4 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 4; NoTakToLogic.col = 4;
            }
	}
	else if(FlowManager.BoardLevel.Equals("5/5"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(2).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 5 X 5 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 5; SolitaireLogic.col = 5;
		}
			
		else
		{
			GameText.text = "NOTAKTO [ 5 X 5 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 5; NoTakToLogic.col = 5;
            }
	}
	else if(FlowManager.BoardLevel.Equals("6/6"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(3).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(3).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 6 X 6 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 6; SolitaireLogic.col = 6;
		}
			
		else
		{
			GameText.text = "NOTAKTO [ 6 X 6 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 6; NoTakToLogic.col = 6;
            }
	}
	else if(FlowManager.BoardLevel.Equals("7/7"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(4).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(4).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 7 X 7 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 7; SolitaireLogic.col = 7;

		}
			
		else
		{
			GameText.text = "NOTAKTO [ 7 X 7 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 7; NoTakToLogic.col = 7;
            }	
			}
	else if(FlowManager.BoardLevel.Equals("8/8"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(5).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(5).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 8 X 8 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 8; SolitaireLogic.col = 8;
		}
			
		else
		{
			GameText.text = "NOTAKTO [ 8 X 8 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 8; NoTakToLogic.col = 8;
            }	
		}
	else if(FlowManager.BoardLevel.Equals("9/9"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(6).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(6).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 9 X 9 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 9; SolitaireLogic.col = 9;

		}
			
		else
		{
			GameText.text = "NOTAKTO [ 9 X 9 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 9; NoTakToLogic.col = 9;
            }
	}
	else if(FlowManager.BoardLevel.Equals("10/10"))
	{
		GridSelected = GamePanel.GetComponent<RectTransform>().GetChild(7).gameObject;
		GamePanel.GetComponent<RectTransform>().GetChild(7).gameObject.SetActive(true);
		if(FlowManager.GameName.Equals("Solitare"))
		{
			GameText.text = "SOLITAIRE [ 10 X 10 ]";
			ScriptHandler.GetComponent<SolitaireLogic>().GamePanel = GridSelected;
			SolitaireLogic.row = 10; SolitaireLogic.col = 10;
		}
			
		else
		{
			GameText.text = "NOTAKTO [ 10 X 10 ]";
			//Header.GetComponent<RectTransform>().GetChild(2).gameObject.SetActive(false);
			Numbered.SetActive(false);
                ScriptHandler.GetComponent<NoTakToLogic>().GamePanel = GridSelected;
                NoTakToLogic.row = 10; NoTakToLogic.col = 10;
            }

            
        }
}
	// Use this for initialization
	void Start () {

		if(!SceneManager.GetActiveScene().name.Equals("GamePlay"))
		{
			SinglePlayer = false; FristPlayerName = "First";
		}
		else
		{
			if (FlowManager.GameName.Equals("NoTak"))
            {
                // ScriptHandler.GetComponent<NoTakToLogic>().enabled = true;
				// ScriptHandler.GetComponent<SolitaireLogic>().enabled = false;
            }
			else
			{
				//ScriptHandler.GetComponent<SolitaireLogic>().enabled = true;
				//ScriptHandler.GetComponent<NoTakToLogic>().enabled = false;
			}
		}
			
		
		if(RulesText[0] == null && RulesText[1] == null)
		return;

		
		if(FlowManager.GameName.Equals("Solitare"))
		{
			RulesText[0].SetActive(true);
		}
		else
		{
			RulesText[1].SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			GameOVR.SetBool("GameOver",false);
		}
	}

	public void ShowRules()
	{
		if(allowed)
		Header.SetActive(false);
		Rules.SetBool("Rule",true);
	}
	public void HideRules()
	{
		
		Rules.SetBool("Rule",false);
		if(allowed)
		Header.SetActive(true);
			
	}


	//
	public void ShowShop()
	{
		if(allowed)
		Header.SetActive(false);
		Shop.SetBool("Rule",true);
	}
	public void HideShop()
	{
		
		Shop.SetBool("Rule",false);
		if(allowed)
		Header.SetActive(true);
			
	}
//
	public void GoToHome()
	{
		Initiate.Fade("StartScene",Color.black,2.0f);
	}

	public void ShowGameOverAnim()

	{
		GameOVR.SetBool("GameOver",true);
	}

	public void ChangeToMultiPlayer()
	{
		SinglePlayer = false;
		PlaywithUser.sprite = selected; PlaywithComp.sprite = normal;
		TurnStartWith.SetActive(true);
	}

	public void ChangeToSinglePlayer()
	{
		SinglePlayer = true;
		PlaywithUser.sprite = normal; PlaywithComp.sprite = selected;
		//TurnStartWith.SetActive(false);
	}

	public void SelectFirstTurn()
	{
		FristPlayerName = "First";
		TurnUser.sprite  = selected; TurnOther.sprite = normal;
	}

	public void SelectSecTurn()
	{
		FristPlayerName = "Second";
		TurnUser.sprite  = normal; TurnOther.sprite = selected;
	}
}
