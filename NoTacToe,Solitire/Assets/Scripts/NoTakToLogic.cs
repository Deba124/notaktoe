﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NoTakToLogic : MonoBehaviour {
public static int row, col;
bool machineTurn = false,InputIsNotAllowed = false,Userturn = true;
bool MachineStillNotGiven = false;
bool GameOver = false,NoMoreSingleLeft = false,DontCheckFurther = false,AllSingleCheckDone = true;
[SerializeField]
public Text AvailableCellCount,CellSearch;
int cellSearchCounter;
public Sprite UserSprite,MachineSprite,unSelectedSprite;
GameObject ins;
public GameObject GamePanel;
public SolitaireLogic solitaire;
public GameObject blink;
[SerializeField]
GameObject GameOverPanel;
GameObject[,] GridMatrix;
GameObject[] AvailableCellHolder;
int MainHolder;
public GameObject[] DiagonalTopLeft,DiagonalTopRight,DiagonalBottomLeft,DiagonalBottomRight,VerticalTop,VerticalBottom,HorizontalLeft,HorizontalRight = new GameObject[2];
int Height, width, k=0, TotalAvailableCell,userOccupied,machineOccupied;
int posRow, posCol;
bool ComputerHasNotGiven = false;
int HoldInstanceRow, HoldInstanceCol;
bool FirstUserTurn;

public Text User,Machine;

	// Use this for initialization
	IEnumerator Start () 
	{
		if(!FlowManager.GameName.Equals("Solitare"))
		{
		
		int m =0;
		for (int i=0; i<row; i++)
		{
			for(int j=0; j<col; j++)
			{
				GamePanel.GetComponent<RectTransform>().GetChild(m).gameObject.GetComponent<Image>().sprite = unSelectedSprite;
				//Debug.Log(GridMatrix[i,j]);
				m++;
			}
		}
        Debug.Log(GsmeController.SinglePlayer + " "+GsmeController.FristPlayerName);
		yield return new WaitForSeconds(.2f);
		if(GsmeController.FristPlayerName.Equals("First"))
		{
			FirstUserTurn = true;
		}
		else
		{
			FirstUserTurn = false;
		}
        Height = row;
        width = col;
       // Debug.Log("Both matrix size is: " + GridMatrix.Length + " " + AvailableCellHolder.Length);
        Debug.Log(Height + " " + width + " " + GamePanel.name);
        GridMatrix = new GameObject[row, col];
        AvailableCellHolder = new GameObject[row * col];
        for (int i=0; i<Height; i++)
		{
			for(int j=0; j<width; j++)
			{
				GridMatrix[i,j] = GamePanel.GetComponent<RectTransform>().GetChild(k).gameObject;
				//Debug.Log(GridMatrix[i,j]);
				k++;
			}
		}
        TotalAvailableCell = Height * width;

		//if(AllSingleCheckDone)
        AvailableCellCount.text = "Available Cell: " + TotalAvailableCell.ToString();
		//else
		//AvailableCellCount.text = "Available Cell: " + TotalAvailableCell.ToString();
		yield return new WaitForSeconds(.2f);
		
		if(GsmeController.SinglePlayer && GsmeController.FristPlayerName.Equals("Second"))
		{  
			ComputerHasNotGiven = true;
			PossibilityCheckForMachine();
			FasterCheck();
		}

		}
    }
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	
	void MakeSlectionInBoard(string member,GameObject gmObj)
	{
		
		if(member.Equals("User") || member.Equals("Machine"))
		{
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			if(member.Equals("Machine"))
			{
				machineTurn = false;
				machineOccupied++;
				ComputerHasNotGiven = false;
				GameObject blnk = Instantiate(blink);
				blnk.GetComponent<Image>().color = Color.blue;
				blnk.GetComponent<RectTransform>().SetParent(gmObj.GetComponent<RectTransform>());
				blnk.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
				blnk.GetComponent<RectTransform>().sizeDelta = new Vector2(gmObj.GetComponent<RectTransform>().sizeDelta.x,gmObj.GetComponent<RectTransform>().sizeDelta.y);
				Destroy(blnk,1f);
				
			}
			else
			{
				userOccupied++;
			}
		}
		else if(member.Equals("Computer"))
		{
			MachineStillNotGiven = false;
			gmObj.GetComponent<Image>().sprite = MachineSprite;
			GameObject blnk = Instantiate(blink);
			blnk.GetComponent<RectTransform>().SetParent(gmObj.GetComponent<RectTransform>());
			blnk.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			blnk.GetComponent<RectTransform>().sizeDelta = new Vector2(gmObj.GetComponent<RectTransform>().sizeDelta.x,gmObj.GetComponent<RectTransform>().sizeDelta.y);
			Destroy(blnk,1f);
			gmObj.tag = "Blocked";
            
		}

		else if(member.Equals("First"))
		{
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			userOccupied++;
		}
		else if(member.Equals("Second"))
		{
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			machineOccupied++;
			//machineOccupied++;
		}

    TotalAvailableCell--;
	AvailableCellCount.text = "Available Cell: " + TotalAvailableCell.ToString();
	
	if(!member.Equals("Computer"))
	FindAdjacent(gmObj);
	
}
	
	public void UserTurn(GameObject gmObj)
	{
		if(FlowManager.GameName.Equals("Solitare"))
		{
			solitaire.UserTurn(gmObj);
			return;
		}
		Debug.Log("User is giving turn");
		if(gmObj.tag.Equals("Selected") || gmObj.tag.Equals("Blocked") || MachineStillNotGiven || InputIsNotAllowed || ComputerHasNotGiven)
		{
			return;
		}
		Userturn = false;
		if(GsmeController.SinglePlayer)
		{
			
			machineTurn = true;
			MakeSlectionInBoard("User",gmObj);
			ComputerHasNotGiven = true;
			PossibilityCheckForMachine();
			
		}
		else
		{
			if(FirstUserTurn)
			{
				MakeSlectionInBoard("First",gmObj);
			}
			else
			{
				MakeSlectionInBoard("Second",gmObj);
			}
			TurnDecider();
		}
			SuperAI();
        	CheckIfWinOrLoss();
			
			if(!GameOver && GsmeController.SinglePlayer)
			FasterCheck();
		
	}

	void TurnDecider()
	{
		if(FirstUserTurn)
		{
			FirstUserTurn = false;
		}
		else
		{
			FirstUserTurn = true;
		}
	}

	void PossibilityCheckForMachine()
	{
		Debug.Log("Possibility");
		MainHolder = 0;
		for(int i =0; i<Height; i++)
		{
			for(int j=0; j<width; j++)
			{
				if(!GridMatrix[i,j].tag.Equals("Selected")&&!GridMatrix[i,j].tag.Equals("Blocked"))
				{
					AvailableCellHolder[MainHolder] = GridMatrix[i,j];
					//Debug.Log("Available: "+AvailableCellHolder[MainHolder]);
					MainHolder++;
				}
			}
		}
		TotalAvailableCell = MainHolder;
		AvailableCellCount.text = "Available Cell: " + TotalAvailableCell.ToString();
		Debug.Log(MainHolder.ToString()+" Main holder length");
	}

	void CheckIfWinOrLoss()
	{
		Debug.Log("WinLoss");
        GameOver = false;
		//Diagonal Check 1
		// 

        if(TotalAvailableCell == 0 && !GameOver)
        {
            GameOver = true;
			Debug.Log("Game Over");
			GameOverPanel.SetActive(true);
			InputIsNotAllowed = true;
            
			// if(userOccupied > machineOccupied)
			// {
			// 	if(GsmeController.SinglePlayer)
			// 	{
			// 		User.text = "You win";
			// 		Machine.text = "Computer loss";
			// 	}
			// 	else
			// 	{
			// 		User.text = "User 1 win";
			// 		Machine.text = "User 2 loss";
			// 	}
				
			// }	
            // else if(machineOccupied > userOccupied)
			// {
			// 	if(GsmeController.SinglePlayer)
			// 	{
			// 		User.text = "You loss";
			// 		Machine.text = "Computer win";
			// 	}
			// 	else
			// 	{
			// 		User.text = "User 1 loss";
			// 		Machine.text = "User 2 win";
			// 	}
			// }
				if(GsmeController.SinglePlayer)
				{
					if(machineTurn)
					{
						User.text = "You win";
						Machine.text = "Computer loss";
					}
					else
					{
						User.text = "You loss";
						Machine.text = "Computer win";
					}
				}
				else
				{
					if(FirstUserTurn)
					{
						User.text = "User 1 loss";
						Machine.text = "User 2 win";
					}
					else
					{
						User.text = "User 1 win";
						Machine.text = "User 2 loss";
					}
				}
			
		}
        FlashOut();
        
		
	}

    void FlashOut()
    {
		Debug.Log("falsh");
        DiagonalTopLeft[0] = DiagonalTopRight[0] = DiagonalBottomLeft[0] = DiagonalBottomRight[0] = VerticalTop[0] = VerticalBottom[0] = HorizontalLeft[0] = HorizontalRight[0] = null;
        DiagonalTopLeft[1] = DiagonalTopRight[1] = DiagonalBottomLeft[1] = DiagonalBottomRight[1] = VerticalTop[1] = VerticalBottom[1] = HorizontalLeft[1] = HorizontalRight[1] = null;

    }

    void FindAdjacent(GameObject Selected,bool MachineChecking = false)
	{ 
		Debug.Log("Adjacent");
		bool found = false;
		
		//Search which cell selected
		for(int i=0; i<Height; i++)
		{
			for(int j=0; j<width; j++)
			{
				if(Selected == GridMatrix[i,j])
				{
					
					posRow = i;posCol =j;
					HoldInstanceRow = posRow; HoldInstanceCol = posCol;
					found = true;
					break;
					
				}
			}
			if(found)
			break;
		}
        
		k=0;
		//Finding DiagonalTopLeft
		while(posRow > 0 && posCol>0)
		{
			DiagonalTopLeft[k] = GridMatrix[--posRow,--posCol];
		//	Debug.Log("DiagonalTopLeft "+ DiagonalTopLeft[k].name);
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;

		//Finding DiagonalTopRight
		while(posRow > 0 && posCol<Height-1)
		{
			DiagonalTopRight[k] = GridMatrix[--posRow,++posCol];
			//Debug.Log("DiagonalTopRight "+ DiagonalTopRight[k].name);
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
	//Finding DiagonalbottomLeft
		while(posRow < Height-1 && posCol > 0)
		{
			DiagonalBottomLeft[k] = GridMatrix[++posRow,--posCol];
			//Debug.Log("DiagonalBottomLeft "+ DiagonalBottomLeft[k].name);			
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding DiagonalbottomRight
		while(posRow < Height-1 && posCol < Height-1)
		{
			DiagonalBottomRight[k] = GridMatrix[++posRow,++posCol];
			//Debug.Log("DiagonalBottomRight "+ DiagonalBottomRight[k].name);
			k++;
						
			if(k == 2)
			{
				break;
			}
		}
		
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding Horizontal Right
		while(posCol < Height -1)
		{
			HorizontalRight[k] = GridMatrix[posRow,++posCol];
		//	Debug.Log("HorizontalRight "+ HorizontalRight[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding Horizontal Left
		while(posCol > 0)
		{
			HorizontalLeft[k] = GridMatrix[posRow,--posCol];
			//Debug.Log("HorizontalLeft "+ HorizontalLeft[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding vertical Top
		while(posRow > 0)
		{
			VerticalTop[k] = GridMatrix[--posRow,posCol];
			//Debug.Log("VerticalTop "+ VerticalTop[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding vertical Bottom
		while(posRow < Height -1)
		{
			VerticalBottom[k] = GridMatrix[++posRow,posCol];
			//Debug.Log("VerticalBottom "+ VerticalBottom[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
	
        

		// if (machineTurn && !Userturn)
		// {
		// 	machineTurn = false;
		// 	Userturn = true;
		// 	Debug.Log("Will check for machine");	
		// 	FasterCheck();
			
		// }
	}

	void FasterCheck()
	{
	  Debug.Log("Machine Turn");
      cellSearchCounter = 0;
		     
	  ins = AvailableCellHolder[Random.Range(0,TotalAvailableCell)];
	    Debug.Log(ins.name + " Is selected by machine");
         if (!ins.tag.Equals("Selected") && !ins.tag.Equals("Blocked"))
        {
            Debug.Log("Machine has selected: " + ins.name);
            MakeSlectionInBoard("Machine",ins); 
			SuperAI();
        	CheckIfWinOrLoss();                   
        }
		   		
	}
    void SuperAI(bool JustChecking = false)
    {
        if (TotalAvailableCell > 0)
        {
			Debug.Log("Ai Check");
			MachineStillNotGiven = true;

		 
			Debug.Log("Now dual checking...");

            if (DiagonalTopLeft[0] != null && DiagonalTopLeft[1] != null)
            {
                if (DiagonalTopLeft[0].tag.Equals("Selected") && !DiagonalTopLeft[1].tag.Equals("Selected")&& !DiagonalTopLeft[1].tag.Equals("Blocked")&& !DiagonalTopLeft[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",DiagonalTopLeft[1]);
                }
				else if(!DiagonalTopLeft[0].tag.Equals("Selected") && DiagonalTopLeft[1].tag.Equals("Selected") && !DiagonalTopLeft[0].tag.Equals("Blocked")&& !DiagonalTopLeft[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",DiagonalTopLeft[0]);
				}
            }

            if (DiagonalBottomLeft[0] != null && DiagonalBottomLeft[1] != null)
            {
                if (DiagonalBottomLeft[0].tag.Equals("Selected") && !DiagonalBottomLeft[1].tag.Equals("Selected")&& !DiagonalBottomLeft[1].tag.Equals("Blocked")&& !DiagonalBottomLeft[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                     MakeSlectionInBoard("Computer",DiagonalBottomLeft[1]);

                }

				else if(!DiagonalBottomLeft[0].tag.Equals("Selected") && DiagonalBottomLeft[1].tag.Equals("Selected")&&!DiagonalBottomLeft[0].tag.Equals("Blocked")&&!DiagonalBottomLeft[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					 MakeSlectionInBoard("Computer",DiagonalBottomLeft[0]);
				}
            }

            if (DiagonalTopRight[0] != null && DiagonalTopRight[1] != null)
            {
                if (DiagonalTopRight[0].tag.Equals("Selected") && !DiagonalTopRight[1].tag.Equals("Selected")&& !DiagonalTopRight[1].tag.Equals("Blocked")&& !DiagonalTopRight[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                   MakeSlectionInBoard("Computer",DiagonalTopRight[1]);
                }
				else if(!DiagonalTopRight[0].tag.Equals("Selected") && DiagonalTopRight[1].tag.Equals("Selected")&&!DiagonalTopRight[0].tag.Equals("Blocked")&&!DiagonalTopRight[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",DiagonalTopRight[0]);
				}
            }

            if (DiagonalBottomRight[0] != null && DiagonalBottomRight[1] != null)
            {
                if (DiagonalBottomRight[0].tag.Equals("Selected") && !DiagonalBottomRight[1].tag.Equals("Selected")&& !DiagonalBottomRight[1].tag.Equals("Blocked")&& !DiagonalBottomRight[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",DiagonalBottomRight[1]);
                }
				else if(!DiagonalBottomRight[0].tag.Equals("Selected") && DiagonalBottomRight[1].tag.Equals("Selected")&&!DiagonalBottomRight[0].tag.Equals("Blocked")&&!DiagonalBottomRight[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",DiagonalBottomRight[0]);
				}
            }
            if (HorizontalRight[0] != null && HorizontalRight[1] != null)
            {
                if (HorizontalRight[0].tag.Equals("Selected") && !HorizontalRight[1].tag.Equals("Selected")&& !HorizontalRight[1].tag.Equals("Blocked")&& !HorizontalRight[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",HorizontalRight[1]);
                }
				else if(!HorizontalRight[0].tag.Equals("Selected") && HorizontalRight[1].tag.Equals("Selected")&&!HorizontalRight[0].tag.Equals("Blocked")&&!HorizontalRight[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",HorizontalRight[0]);
				}
            }
            if (HorizontalLeft[0] != null && HorizontalLeft[1] != null)
            {
                if (HorizontalLeft[0].tag.Equals("Selected") && !HorizontalLeft[1].tag.Equals("Selected")&& !HorizontalLeft[1].tag.Equals("Blocked")&& !HorizontalLeft[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",HorizontalLeft[1]);
                }
				else if(!HorizontalLeft[0].tag.Equals("Selected") && HorizontalLeft[1].tag.Equals("Selected")&&!HorizontalLeft[0].tag.Equals("Blocked")&&!HorizontalLeft[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",HorizontalLeft[0]);
				}
            }

            if (VerticalTop[0] != null && VerticalTop[1] != null)
            {
                if (VerticalTop[0].tag.Equals("Selected") && !VerticalTop[1].tag.Equals("Selected")&& !VerticalTop[1].tag.Equals("Blocked")&& !VerticalTop[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",VerticalTop[1]);
                }
				else if(!VerticalTop[0].tag.Equals("Selected") && VerticalTop[1].tag.Equals("Selected") && !VerticalTop[0].tag.Equals("Blocked")&& !VerticalTop[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",VerticalTop[0]);
				}
            }

            if (VerticalBottom[0] != null && VerticalBottom[1] != null)
            {
                if (VerticalBottom[0].tag.Equals("Selected") && !VerticalBottom[1].tag.Equals("Selected")&& !VerticalBottom[1].tag.Equals("Blocked")&& !VerticalBottom[1].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",VerticalBottom[1]);

                }
				else if(!VerticalBottom[0].tag.Equals("Selected") && VerticalBottom[1].tag.Equals("Selected")&&!VerticalBottom[0].tag.Equals("Blocked")&&!VerticalBottom[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",VerticalBottom[0]);
				}
            }

            if (VerticalTop[0] != null && VerticalBottom[0] != null)
            {
                if (VerticalTop[0].tag.Equals("Selected") && !VerticalBottom[0].tag.Equals("Selected")&& !VerticalBottom[0].tag.Equals("Blocked")&& !VerticalBottom[0].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",VerticalBottom[0]);

                }
				else if(!VerticalTop[0].tag.Equals("Selected") && VerticalBottom[0].tag.Equals("Selected")&&!VerticalTop[0].tag.Equals("Blocked")&&!VerticalTop[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",VerticalTop[0]);
				}
            }
            if (HorizontalLeft[0] != null && HorizontalRight[0] != null)
            {
                if (HorizontalLeft[0].tag.Equals("Selected") && !HorizontalRight[0].tag.Equals("Selected")&& !HorizontalRight[0].tag.Equals("Blocked")&& !HorizontalRight[0].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",HorizontalRight[0]);

                }
				else if(!HorizontalLeft[0].tag.Equals("Selected") && HorizontalRight[0].tag.Equals("Selected")&&!HorizontalLeft[0].tag.Equals("Blocked")&&!HorizontalLeft[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",HorizontalLeft[0]);
				}
            }
            if (DiagonalTopLeft[0] != null && DiagonalBottomRight[0] != null)
            {
                if (DiagonalTopLeft[0].tag.Equals("Selected") && !DiagonalBottomRight[0].tag.Equals("Selected")&& !DiagonalBottomRight[0].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",DiagonalBottomRight[0]);
                }
				else if(!DiagonalTopLeft[0].tag.Equals("Selected") && DiagonalBottomRight[0].tag.Equals("Selected")&&!DiagonalTopLeft[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",DiagonalTopLeft[0]);
				}
            }
            if (DiagonalTopRight[0] != null && DiagonalBottomLeft[0] != null)
            {
                if (DiagonalTopRight[0].tag.Equals("Selected") && !DiagonalBottomLeft[0].tag.Equals("Selected")&& !DiagonalBottomLeft[0].tag.Equals("Blocked"))
                {
					if(!JustChecking)
                    MakeSlectionInBoard("Computer",DiagonalBottomLeft[0]);
                }
				else if(!DiagonalTopRight[0].tag.Equals("Selected") && DiagonalBottomLeft[0].tag.Equals("Selected") && !DiagonalTopRight[0].tag.Equals("Blocked"))
				{
					if(!JustChecking)
					MakeSlectionInBoard("Computer",DiagonalTopRight[0]);
				}

            }
			MachineStillNotGiven = false;
            FlashOut();
            PossibilityCheckForMachine();

		}
    }

	IEnumerator SpecialAiCheckForComputer(GameObject obj)
	{
		yield return null;

	}

    public void Reset()
    {
        SceneManager.LoadScene("GamePlay");
    }

}