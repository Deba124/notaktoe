﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolitaireLogic : MonoBehaviour {
int fontSize;
public static int row, col;
bool useNum = false;
bool machineTurn = false,InputIsNotAllowed = false,Userturn = true;
bool MachineStillNotGiven = false;
bool GameOver = false,NoMoreSingleLeft = false,DontCheckFurther = false,AllSingleCheckDone = true;
[SerializeField]
public Text AvailableCellCount,CellSearch;
int cellSearchCounter;
public Sprite UserSprite,MachineSprite;
GameObject ins;
public GameObject GamePanel;
public GameObject blink;
[SerializeField]
GameObject GameOverPanel;
GameObject[,] GridMatrix;
GameObject[] AvailableCellHolder;
int MainHolder;
public GameObject[] DiagonalTopLeft,DiagonalTopRight,DiagonalBottomLeft,DiagonalBottomRight,VerticalTop,VerticalBottom,HorizontalLeft,HorizontalRight = new GameObject[2];
int Height, width, k=0, TotalAvailableCell,userOccupied,machineOccupied;
int posRow, posCol;
bool ComputerHasNotGiven = false;
int HoldInstanceRow, HoldInstanceCol;
bool FirstUserTurn;
public GameObject NumberText;
public Sprite[] SelectUnselect = new Sprite[2];

public Text User,Machine;
	// Use this for initialization
	IEnumerator Start () 
	{

		yield return new WaitForSeconds(.2f);
		if(FlowManager.GameName.Equals("Solitare"))
		{
		Height = row;
        width = col;
		GridMatrix = new GameObject[row, col];
		AvailableCellHolder = new GameObject[row * col];
        for (int i=0; i<Height; i++)
		{
			for(int j=0; j<width; j++)
			{
				GridMatrix[i,j] = GamePanel.GetComponent<RectTransform>().GetChild(k).gameObject;
				Debug.Log(GridMatrix[i,j].name);
				k++;
			}
		}
        TotalAvailableCell = Height * width;

		if(row == 3)
		fontSize = 100;
		if(row == 4)
		fontSize = 90;
		if(row == 5)
		fontSize = 80;
		if(row == 6)
		fontSize = 70;
		if(row == 7)
		fontSize = 60;
		if(row == 8)
		fontSize = 55;
		if(row == 9)
		fontSize = 50;
		if(row == 10)
		fontSize = 50;

		}
	}
	
	public void UserTurn(GameObject gmObj)
	{
		Debug.Log("User is giving turn");
		if(gmObj.tag.Equals("Selected") || gmObj.tag.Equals("Blocked") || MachineStillNotGiven || InputIsNotAllowed || ComputerHasNotGiven)
		{
			return;
		}

		MakeSlectionInBoard("User",gmObj);
		FindAdjacent(gmObj);
		
		if(useNum)
		Numbering();
		
		SuperAI();
	}
	
	void SuperAI()
    {
       	Debug.Log("Ai Check");
			MachineStillNotGiven = true;
			Debug.Log("Now dual checking...");

            if (DiagonalTopLeft[0] != null && DiagonalTopLeft[1] != null)
            {
                if (DiagonalTopLeft[0].tag.Equals("Selected") && !DiagonalTopLeft[1].tag.Equals("Selected")&& !DiagonalTopLeft[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",DiagonalTopLeft[1]);
                }
				else if(!DiagonalTopLeft[0].tag.Equals("Selected") && DiagonalTopLeft[1].tag.Equals("Selected") && !DiagonalTopLeft[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",DiagonalTopLeft[0]);
				}
            }

            if (DiagonalBottomLeft[0] != null && DiagonalBottomLeft[1] != null)
            {
                if (DiagonalBottomLeft[0].tag.Equals("Selected") && !DiagonalBottomLeft[1].tag.Equals("Selected")&& !DiagonalBottomLeft[1].tag.Equals("Blocked"))
                {
                     MakeSlectionInBoard("Computer",DiagonalBottomLeft[1]);

                }

				else if(!DiagonalBottomLeft[0].tag.Equals("Selected") && DiagonalBottomLeft[1].tag.Equals("Selected")&&!DiagonalBottomLeft[0].tag.Equals("Blocked"))
				{
					 MakeSlectionInBoard("Computer",DiagonalBottomLeft[0]);
				}
            }

            if (DiagonalTopRight[0] != null && DiagonalTopRight[1] != null)
            {
                if (DiagonalTopRight[0].tag.Equals("Selected") && !DiagonalTopRight[1].tag.Equals("Selected")&& !DiagonalTopRight[1].tag.Equals("Blocked"))
                {
                   MakeSlectionInBoard("Computer",DiagonalTopRight[1]);
                }
				else if(!DiagonalTopRight[0].tag.Equals("Selected") && DiagonalTopRight[1].tag.Equals("Selected")&&!DiagonalTopRight[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",DiagonalTopRight[0]);
				}
            }

            if (DiagonalBottomRight[0] != null && DiagonalBottomRight[1] != null)
            {
                if (DiagonalBottomRight[0].tag.Equals("Selected") && !DiagonalBottomRight[1].tag.Equals("Selected")&& !DiagonalBottomRight[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",DiagonalBottomRight[1]);
                }
				else if(!DiagonalBottomRight[0].tag.Equals("Selected") && DiagonalBottomRight[1].tag.Equals("Selected")&&!DiagonalBottomRight[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",DiagonalBottomRight[0]);
				}
            }
            if (HorizontalRight[0] != null && HorizontalRight[1] != null)
            {
                if (HorizontalRight[0].tag.Equals("Selected") && !HorizontalRight[1].tag.Equals("Selected")&& !HorizontalRight[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",HorizontalRight[1]);
                }
				else if(!HorizontalRight[0].tag.Equals("Selected") && HorizontalRight[1].tag.Equals("Selected")&&!HorizontalRight[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",HorizontalRight[0]);
				}
            }
            if (HorizontalLeft[0] != null && HorizontalLeft[1] != null)
            {
                if (HorizontalLeft[0].tag.Equals("Selected") && !HorizontalLeft[1].tag.Equals("Selected")&& !HorizontalLeft[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",HorizontalLeft[1]);
                }
				else if(!HorizontalLeft[0].tag.Equals("Selected") && HorizontalLeft[1].tag.Equals("Selected")&&!HorizontalLeft[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",HorizontalLeft[0]);
				}
            }

            if (VerticalTop[0] != null && VerticalTop[1] != null)
            {
                if (VerticalTop[0].tag.Equals("Selected") && !VerticalTop[1].tag.Equals("Selected")&& !VerticalTop[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",VerticalTop[1]);
                }
				else if(!VerticalTop[0].tag.Equals("Selected") && VerticalTop[1].tag.Equals("Selected") && !VerticalTop[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",VerticalTop[0]);
				}
            }

            if (VerticalBottom[0] != null && VerticalBottom[1] != null)
            {
                if (VerticalBottom[0].tag.Equals("Selected") && !VerticalBottom[1].tag.Equals("Selected")&& !VerticalBottom[1].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",VerticalBottom[1]);

                }
				else if(!VerticalBottom[0].tag.Equals("Selected") && VerticalBottom[1].tag.Equals("Selected")&&!VerticalBottom[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",VerticalBottom[0]);
				}
            }

            if (VerticalTop[0] != null && VerticalBottom[0] != null)
            {
                if (VerticalTop[0].tag.Equals("Selected") && !VerticalBottom[0].tag.Equals("Selected")&& !VerticalBottom[0].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",VerticalBottom[0]);

                }
				else if(!VerticalTop[0].tag.Equals("Selected") && VerticalBottom[0].tag.Equals("Selected")&&!VerticalTop[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",VerticalTop[0]);
				}
            }
            if (HorizontalLeft[0] != null && HorizontalRight[0] != null)
            {
                if (HorizontalLeft[0].tag.Equals("Selected") && !HorizontalRight[0].tag.Equals("Selected")&& !HorizontalRight[0].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",HorizontalRight[0]);

                }
				else if(!HorizontalLeft[0].tag.Equals("Selected") && HorizontalRight[0].tag.Equals("Selected")&&!HorizontalLeft[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",HorizontalLeft[0]);
				}
            }
            if (DiagonalTopLeft[0] != null && DiagonalBottomRight[0] != null)
            {
                if (DiagonalTopLeft[0].tag.Equals("Selected") && !DiagonalBottomRight[0].tag.Equals("Selected")&& !DiagonalBottomRight[0].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",DiagonalBottomRight[0]);
                }
				else if(!DiagonalTopLeft[0].tag.Equals("Selected") && DiagonalBottomRight[0].tag.Equals("Selected")&&!DiagonalTopLeft[0].tag.Equals("Blocked"))
				{
					MakeSlectionInBoard("Computer",DiagonalTopLeft[0]);
				}
            }
            if (DiagonalTopRight[0] != null && DiagonalBottomLeft[0] != null)
            {
                if (DiagonalTopRight[0].tag.Equals("Selected") && !DiagonalBottomLeft[0].tag.Equals("Selected")&& !DiagonalBottomLeft[0].tag.Equals("Blocked"))
                {
                    MakeSlectionInBoard("Computer",DiagonalBottomLeft[0]);
                }
				else if(!DiagonalTopRight[0].tag.Equals("Selected") && DiagonalBottomLeft[0].tag.Equals("Selected") && !DiagonalTopRight[0].tag.Equals("Selected"))
				{
					MakeSlectionInBoard("Computer",DiagonalTopRight[0]);
				}

            }
			MachineStillNotGiven = false;
            FlashOut();
            //PossibilityCheckForMachine();

    }

	void FlashOut()
    {
		Debug.Log("falsh");
        DiagonalTopLeft[0] = DiagonalTopRight[0] = DiagonalBottomLeft[0] = DiagonalBottomRight[0] = VerticalTop[0] = VerticalBottom[0] = HorizontalLeft[0] = HorizontalRight[0] = null;
        DiagonalTopLeft[1] = DiagonalTopRight[1] = DiagonalBottomLeft[1] = DiagonalBottomRight[1] = VerticalTop[1] = VerticalBottom[1] = HorizontalLeft[1] = HorizontalRight[1] = null;

    }

	public void SelectNumbering(Image obj)
	{
		if(!useNum)
		{
			useNum = true;
			obj.sprite = SelectUnselect[0];
			Numbering();
			
		}
		else
		{
			useNum = false;
			obj.sprite = SelectUnselect[1];
			Numbering(true);
			
		}
	}

public void Numbering(bool remove = false)
	{
		
		int counter = 0;
		for(int i = 0; i<Height; i++)
		{
			for(int j =0; j<width; j++)
			{
				if(!remove)
				{
					if(GridMatrix[i,j].tag.Equals("Selected") && !GridMatrix[i,j].tag.Equals("Blocked"))
					{
					NumberText.GetComponent<Text>().text = (++counter).ToString();
					NumberText.GetComponent<Text>().fontSize = fontSize;
					if(GridMatrix[i,j].GetComponent<RectTransform>().childCount == 0)
					{
					GameObject obj = Instantiate(NumberText);
					obj.GetComponent<RectTransform>().SetParent(GridMatrix[i,j].GetComponent<RectTransform>());
					obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
					//obj.GetComponent<RectTransform>().localScale.;
					}
					else
					{
						GridMatrix[i,j].GetComponent<RectTransform>().GetChild(0).gameObject.GetComponent<Text>().text = counter.ToString();
						GridMatrix[i,j].GetComponent<RectTransform>().GetChild(0).gameObject.GetComponent<Text>().fontSize = fontSize;
					}
					}
				}
				
				else
				{
					if(GridMatrix[i,j].tag.Equals("Selected"))
					GridMatrix[i,j].GetComponent<RectTransform>().GetChild(0).gameObject.GetComponent<Text>().text = "";
				}
			}
		}

	}

	void MakeSlectionInBoard(string member,GameObject gmObj)
	{
		
		if(member.Equals("User") || member.Equals("Machine"))
		{
			Debug.Log("Hitting to chnage sprite solitaire");
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			if(member.Equals("Machine"))
			{
				machineTurn = false;
				machineOccupied++;
				ComputerHasNotGiven = false;
				GameObject blnk = Instantiate(blink);
				blnk.GetComponent<Image>().color = Color.blue;
				blnk.GetComponent<RectTransform>().SetParent(gmObj.GetComponent<RectTransform>());
				blnk.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
				blnk.GetComponent<RectTransform>().sizeDelta = new Vector2(gmObj.GetComponent<RectTransform>().sizeDelta.x,gmObj.GetComponent<RectTransform>().sizeDelta.y);
				Destroy(blnk,1f);
				
			}
			else
			{
				userOccupied++;
			}
		}
		else if(member.Equals("Computer"))
		{
			Debug.Log("Hitting to chnage sprite comp solitaire");
			MachineStillNotGiven = false;
			gmObj.GetComponent<Image>().sprite = MachineSprite;
			GameObject blnk = Instantiate(blink);
			blnk.GetComponent<RectTransform>().SetParent(gmObj.GetComponent<RectTransform>());
			blnk.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			blnk.GetComponent<RectTransform>().sizeDelta = new Vector2(gmObj.GetComponent<RectTransform>().sizeDelta.x,gmObj.GetComponent<RectTransform>().sizeDelta.y);
			Destroy(blnk,1f);
			gmObj.tag = "Blocked";
            
		}

		else if(member.Equals("First"))
		{
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			userOccupied++;
		}
		else if(member.Equals("Second"))
		{
			gmObj.GetComponent<Image>().sprite = UserSprite;
			gmObj.tag = "Selected";
			machineOccupied++;
			//machineOccupied++;
		}

    TotalAvailableCell--;
	AvailableCellCount.text = "Available Cell: " + TotalAvailableCell.ToString();
	
	if(!member.Equals("Computer"))
	FindAdjacent(gmObj);
	 
}
 void FindAdjacent(GameObject Selected,bool MachineChecking = false)
	{ 
		Debug.Log("Adjacent");
		bool found = false;
		
		//Search which cell selected
		for(int i=0; i<Height; i++)
		{
			for(int j=0; j<width; j++)
			{
				if(Selected == GridMatrix[i,j])
				{
					
					posRow = i;posCol =j;
					HoldInstanceRow = posRow; HoldInstanceCol = posCol;
					found = true;
					break;
					
				}
			}
			if(found)
			break;
		}
        
		k=0;
		//Finding DiagonalTopLeft
		while(posRow > 0 && posCol>0)
		{
			DiagonalTopLeft[k] = GridMatrix[--posRow,--posCol];
		//	Debug.Log("DiagonalTopLeft "+ DiagonalTopLeft[k].name);
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;

		//Finding DiagonalTopRight
		while(posRow > 0 && posCol<Height-1)
		{
			DiagonalTopRight[k] = GridMatrix[--posRow,++posCol];
			//Debug.Log("DiagonalTopRight "+ DiagonalTopRight[k].name);
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
	//Finding DiagonalbottomLeft
		while(posRow < Height-1 && posCol > 0)
		{
			DiagonalBottomLeft[k] = GridMatrix[++posRow,--posCol];
			//Debug.Log("DiagonalBottomLeft "+ DiagonalBottomLeft[k].name);			
			k++;
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding DiagonalbottomRight
		while(posRow < Height-1 && posCol < Height-1)
		{
			DiagonalBottomRight[k] = GridMatrix[++posRow,++posCol];
			//Debug.Log("DiagonalBottomRight "+ DiagonalBottomRight[k].name);
			k++;
						
			if(k == 2)
			{
				break;
			}
		}
		
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding Horizontal Right
		while(posCol < Height -1)
		{
			HorizontalRight[k] = GridMatrix[posRow,++posCol];
		//	Debug.Log("HorizontalRight "+ HorizontalRight[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding Horizontal Left
		while(posCol > 0)
		{
			HorizontalLeft[k] = GridMatrix[posRow,--posCol];
			//Debug.Log("HorizontalLeft "+ HorizontalLeft[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding vertical Top
		while(posRow > 0)
		{
			VerticalTop[k] = GridMatrix[--posRow,posCol];
			//Debug.Log("VerticalTop "+ VerticalTop[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
		//Finding vertical Bottom
		while(posRow < Height -1)
		{
			VerticalBottom[k] = GridMatrix[++posRow,posCol];
			//Debug.Log("VerticalBottom "+ VerticalBottom[k].name);
			k++;
			
			if(k == 2)
			{
				break;
			}
		}
		posRow = HoldInstanceRow; posCol = HoldInstanceCol;
		k = 0;
	
        

		// if (machineTurn && !Userturn)
		// {
		// 	machineTurn = false;
		// 	Userturn = true;
		// 	Debug.Log("Will check for machine");	
		// 	FasterCheck();
			
		// }
	}

}

